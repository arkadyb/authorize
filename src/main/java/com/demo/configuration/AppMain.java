package com.demo.configuration;

import com.demo.configuration.filters.CORSResponseFilter;
import org.glassfish.jersey.server.ResourceConfig;

public class AppMain extends ResourceConfig {
    public AppMain() {
        try {
            register(CORSResponseFilter.class);
            register(new AppBinder());
            packages(true, "com.demo.rest");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
