package com.demo.configuration.filters;

import com.demo.services.interfaces.Configuration;
import com.demo.services.interfaces.Logger;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;

public class CORSResponseFilter implements ContainerResponseFilter {

    @Inject
    Logger logger;

    @Inject
    Configuration conf;

    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {

        try{
            MultivaluedMap<String, Object> headers = responseContext.getHeaders();
            headers.add("Access-Control-Allow-Origin", conf.getValue("corsurl"));
            headers.add("Access-Control-Allow-Methods", "GET, POST");
            headers.add("Access-Control-Allow-Headers", "X-Requested-With, X-Forwarded-For, Content-Type, X-Codingpedia, Authorization");
        }catch (Exception e){
            logger.Error(e);
        }
    }
}