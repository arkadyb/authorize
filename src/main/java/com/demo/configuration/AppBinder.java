package com.demo.configuration;

import com.demo.services.*;
import com.demo.services.authorization.DefaultAuthorizationProviderFactory;
import com.demo.services.authorization.interfaces.AuthorizationProviderFactory;
import com.demo.services.interfaces.*;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;

public class AppBinder extends AbstractBinder {

    @Override
    protected void configure() {

        bind(PropertiesConfiguration.class).to(Configuration.class).in(Singleton.class);
        bind(FileSystemLogger.class).to(Logger.class).in(Singleton.class);
        bind(MongoClient.class).to(MongoClient.class).in(Singleton.class);
        bind(DefaultAuthorizationProviderFactory.class).to(AuthorizationProviderFactory.class).in(Singleton.class);
    }
}
