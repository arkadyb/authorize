package com.demo.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
    private static String salt = "ABvd8Hg8H+JQ+2$$6c2w39h4^fQDAhN@9+kt8wjer%f$_K*vj8";

    private static String hex(byte[] array) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
            sb.append(Integer.toHexString((array[i]
                    & 0xFF) | 0x100).substring(1,3));
        }
        return sb.toString();
    }
    public static String ToMD5(String message) {
        try {
            message+=salt;
            MessageDigest md =
                    MessageDigest.getInstance("MD5");
            return hex (md.digest(message.getBytes()));
        } catch (NoSuchAlgorithmException e) {
        }

        return null;
    }

}
