package com.demo.utils;

public class StringUtils {
    public static boolean IsNullOrEmpty(String str){
        if(str != null && !str.trim().isEmpty()){
            return false;
        }

        return true;
    }
}
