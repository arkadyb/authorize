package com.demo.rest;

import com.demo.models.AuthToken;
import com.demo.models.Profile;
import com.demo.rest.models.Token;
import com.demo.rest.models.interfaces.OperationResult;
import com.demo.rest.models.response.Error;
import com.demo.rest.models.response.Success;
import com.demo.services.authorization.interfaces.AuthorizationProvider;
import com.demo.services.authorization.interfaces.AuthorizationProviderFactory;
import com.demo.services.authorization.interfaces.SocialNetworkProfile;
import com.demo.utils.MD5Util;
import com.demo.utils.StringUtils;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.AbstractMap;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/*
    AuthorizationController defines call handlers for user registration and authorization for both - built-in and social network authorizaton.
*/
@Path("users")
public class AuthorizationController extends BaseController{

    @Inject
    AuthorizationProviderFactory authFactory;

    @GET
    @Path("/authorize")
    @Produces(MediaType.APPLICATION_JSON)
    public OperationResult Authorize(@QueryParam("email") String email, @QueryParam("pwd") String pwd){
        try{
            if(StringUtils.IsNullOrEmpty(email) || StringUtils.IsNullOrEmpty(pwd))
                return new Error("error");

            email = email.toLowerCase();
            pwd = MD5Util.ToMD5(pwd);

            AbstractMap.SimpleEntry<String, String>[] query=
                    new AbstractMap.SimpleEntry[]{
                        new AbstractMap.SimpleEntry("email", email),
                        new AbstractMap.SimpleEntry("password", pwd)
                    };

            List<Profile> profiles = db.GetItems(Profile.class, query);
            if(profiles!=null & profiles.size()==1){
                Profile profile = profiles.get(0);

                if (profile!=null){
                    String accessToken = UUID.randomUUID().toString().replaceAll("-", "");

                    Calendar cnr = Calendar.getInstance();
                    cnr.add(Calendar.MONTH, 1);

                    if(profile.getTokens()!=null){
                        for (AuthToken token:
                                profile.getTokens()) {

                            if(token.getType()== AuthorizationProviderFactory.Provider.demo){
                                token.setExpires(cnr.getTime().getTime());
                                token.setToken(accessToken);

                                if(db.UpdateItem(profile))
                                    return new Success<>(new Token(token.getToken(), token.getExpires()));
                            }

                        }
                    }

                    //No token was found in user`s Profile. Create a new one and return its value.
                    AuthToken authToken = new AuthToken(accessToken, cnr.getTime().getTime(), profile.getEntityId().toHexString(), AuthorizationProviderFactory.Provider.demo);
                    profile.getTokens().add(authToken);

                    if(db.UpdateItem(profile))
                        return new Success<>(new Token(authToken.getToken(), authToken.getExpires()));
                }
            }
        }catch (Exception e){
            logger.Error(e);
            return new Error("error");
        }

        return new Success<>("notfound");
    }

    @GET
    @Path("/authorize/{type}")
    @Produces(MediaType.APPLICATION_JSON)
    public OperationResult Authorize(@QueryParam("code") String code, @PathParam("type") AuthorizationProviderFactory.Provider type) {

        try {

            AuthorizationProviderFactory.Provider authProvider= AuthorizationProviderFactory.Provider.unknown;
            switch (type.toString().toLowerCase())
            {
                case "facebook":
                    authProvider = AuthorizationProviderFactory.Provider.facebook;
                    break;
            }

            AuthorizationProvider provider = authFactory.GetProvider(authProvider);

            if (code!=null){
                OAuthAccessTokenResponse tokenResponse = provider.Authorize(code);

                String accessToken = tokenResponse.getAccessToken();
                provider.setAccessToken(accessToken);

                SocialNetworkProfile sProfile = provider.GetUserId();

                AbstractMap.SimpleEntry<String, String>[] c =
                        new AbstractMap.SimpleEntry[]{
                            new AbstractMap.SimpleEntry("tokens.socialId", sProfile.getId()),
                            new AbstractMap.SimpleEntry("tokens.type", authProvider)
                        };

                List<Profile> profiles = db.GetItems(Profile.class, c);
                if(profiles!=null && profiles.size()==1){//Profile found

                    Profile profile = profiles.get(0);
                    if(profile!=null){
                        for (AuthToken token : profile.getTokens()) {

                            if(token.getSocialId().equals(sProfile.getId()) && token.getType() == authProvider){
                                token.setExpires(tokenResponse.getExpiresIn()*1000);
                                token.setToken(accessToken);

                                if(!profile.getFirstName().equals(sProfile.getFirstName()))
                                    profile.setFirstName(sProfile.getFirstName());
                                if(!profile.getLastName().equals(sProfile.getLastName()))
                                    profile.setLastName(sProfile.getLastName());
                                if(!profile.getEmail().equals(sProfile.getEmail()))
                                    profile.setEmail(sProfile.getEmail());

                                db.UpdateItem(profile);

                                return new Success(new Token(token.getToken(), token.getExpires()));
                            }
                        }
                    }
                } else {//Profile not found. Creating one with auth token
                    Profile profile = new Profile();
                    profile.setEmail(sProfile.getEmail());
                    profile.setFirstName(sProfile.getFirstName());
                    profile.setLastName(sProfile.getLastName());

                    AuthToken authToken = new AuthToken(
                            accessToken,
                            tokenResponse.getExpiresIn()*1000,
                            sProfile.getId(),
                            authProvider);

                    profile.getTokens().add(authToken);

                    db.SaveItem(profile);

                    return new Success(new Token(authToken.getToken(), authToken.getExpires()));
                }
            }
            else{
                return new Success(provider.GenerateAuthUrl());
            }

        } catch (Exception e){
            logger.Error(e);

        }

            return new Error("error");
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public OperationResult Register(Profile p){
        try{
            if (p==null){
                return new Error("null");
            }

            if(!StringUtils.IsNullOrEmpty(p.getEmail()) && !StringUtils.IsNullOrEmpty(p.getPassword())){
                p.setEmail(p.getEmail().trim().toLowerCase());
            }else{
                throw new Exception("Email not specified.");
            }

            if(!db.ItemExists(Profile.class, "email", p.getEmail())){
                    p.setPassword(MD5Util.ToMD5(p.getPassword().trim()));

                    if(db.SaveItem(p)!=null)
                        return new Success("ok");
                } else {
                    return new Success("exists");
            }
        }catch (Exception e){
            logger.Error(e);
        }

        return new Error("error");
    }
}
