package com.demo.rest;

import com.demo.services.MongoClient;
import com.demo.services.interfaces.Logger;

import javax.inject.Inject;

public abstract class BaseController {
    @Inject
    MongoClient db;

    @Inject
    Logger logger;
}
