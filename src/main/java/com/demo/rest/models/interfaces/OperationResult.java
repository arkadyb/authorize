package com.demo.rest.models.interfaces;

/*
    OperationResult defines basic rest methods result.
 */
public interface OperationResult {
    boolean getSuccess();
}
