package com.demo.rest.models;

/*
    Token is result object used to send authorization details to client.
 */
public class Token {
    private String token;

    public String getToken() {
        return token;
    }

    private Long expires;

    public Long getExpires() {
        return expires;
    }

    public Token(String token, Long expires){
        this.token = token;
        this.expires = expires;
    }
}
