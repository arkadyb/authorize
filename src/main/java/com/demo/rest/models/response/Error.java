package com.demo.rest.models.response;

import com.demo.rest.models.interfaces.OperationResult;

/*
    OperationResult implementation for failed calls.
 */
public class Error implements OperationResult {

    private String error;

    public String getError(){
        return error;
    }

    public boolean getSuccess(){
        return false;
    }

    public Error(String msg){
        error = msg;
    }

    public Error(Exception e){
        if (e.getMessage()!=null)
            error=e.getMessage();
        else
            error="No error message available.";
    }

}
