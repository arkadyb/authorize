package com.demo.rest.models.response;

import com.demo.rest.models.interfaces.OperationResult;

/*
    OperationResult implementation for successful calls.
 */
public class Success<T> implements OperationResult {

    private T result;

    public boolean getSuccess(){
        return true;
    }

    public T getResult(){
        return result;
    }

    public Success(T e){
        result = e;
    }
}
