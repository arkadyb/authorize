package com.demo.services.authorization.interfaces;

import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

public interface AuthorizationProvider {
    void setAccessToken(String token);
    OAuthAccessTokenResponse Authorize(String code) throws OAuthSystemException, OAuthProblemException;
    String GenerateAuthUrl() throws OAuthSystemException;
    SocialNetworkProfile GetUserId() throws OAuthSystemException, OAuthProblemException;
}
