package com.demo.services.authorization.interfaces;

import com.demo.services.authorization.exceptions.ProviderNotFoundException;

import javax.naming.ConfigurationException;

public interface AuthorizationProviderFactory {
    enum Provider{
        unknown,
        demo,//for local email/pwd authorization
        facebook
    }

    AuthorizationProvider GetProvider(Provider provider) throws ProviderNotFoundException, ConfigurationException;
}
