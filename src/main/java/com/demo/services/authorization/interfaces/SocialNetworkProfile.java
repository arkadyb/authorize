package com.demo.services.authorization.interfaces;

public interface SocialNetworkProfile {
    String getId();
    String getFirstName();
    String getLastName();
    String getEmail();
}
