package com.demo.services.authorization.providers.facebook.responses;

import com.demo.services.authorization.interfaces.SocialNetworkProfile;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class FacebookMeResponse extends OAuthResourceResponse implements SocialNetworkProfile {

    private JSONObject jBody = null;

    public String getId(){
        return jBody.getString("id");
    }

    public String getFirstName(){
        return jBody.getString("first_name");
    }

    public String getLastName(){
        return jBody.getString("last_name");
    }

    public String getEmail(){ return jBody.has("email")?jBody.getString("email"):""; }

    protected void init(InputStream body, String contentType, int responseCode, Map<String, List<String>> headers) throws OAuthProblemException {
        super.init(body,contentType,responseCode,headers);

        jBody = new JSONObject(this.getBody());
    }
}
