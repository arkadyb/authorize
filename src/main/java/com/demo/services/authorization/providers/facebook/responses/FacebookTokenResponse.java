package com.demo.services.authorization.providers.facebook.responses;

import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.token.BasicOAuthToken;
import org.apache.oltu.oauth2.common.token.OAuthToken;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;


public class FacebookTokenResponse extends OAuthAccessTokenResponse {
    public FacebookTokenResponse() {
    }

    public String getAccessToken() {
        return this.getParam("access_token");
    }

    public String getTokenType() {
        return this.getParam("token_type");
    }

    public Long getExpiresIn() {
        String value = this.getParam("expires");
        return value == null?null:Long.valueOf(value);
    }

    public String getRefreshToken() {
        return this.getParam("refresh_token");
    }

    public String getScope() {
            return this.getParam("scope");
    }

    public OAuthToken getOAuthToken() {
        return new BasicOAuthToken(this.getAccessToken(), this.getTokenType(), this.getExpiresIn(), this.getRefreshToken(), this.getScope());
    }

    protected void setBody(String body) {
        this.body = body;
        this.parameters = OAuthUtils.decodeForm(body);
    }

    protected void setContentType(String contentType) {
        this.contentType = contentType;
    }

    protected void setResponseCode(int code) {
        this.responseCode = code;
    }
}
