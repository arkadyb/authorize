package com.demo.services.authorization.providers.facebook;

import com.demo.services.authorization.interfaces.AuthorizationProvider;
import com.demo.services.authorization.interfaces.SocialNetworkProfile;
import com.demo.services.authorization.providers.facebook.responses.FacebookMeResponse;
import com.demo.services.authorization.providers.facebook.responses.FacebookTokenResponse;
import com.demo.services.interfaces.Configuration;
import com.demo.utils.StringUtils;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.OAuthProviderType;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;

import javax.naming.ConfigurationException;

public class FacebookProvider implements AuthorizationProvider {

    private String clientId;
    private String callbackUrl;
    private String clientSecret;
    private String token;

    public void setAccessToken(String token){
        this.token = token;
    }

    public FacebookProvider(Configuration c) throws ConfigurationException{
        clientId = c.getValue("fbclientid");
        callbackUrl = c.getValue("fbcallbackurl");
        clientSecret =c.getValue("fbclientsecret");
    }

    public OAuthAccessTokenResponse Authorize(String code) throws OAuthSystemException, OAuthProblemException{
        OAuthClientRequest request = OAuthClientRequest
                .tokenProvider(OAuthProviderType.FACEBOOK)
                .setGrantType(GrantType.AUTHORIZATION_CODE)
                .setClientId(clientId)
                .setClientSecret(clientSecret)
                .setRedirectURI(callbackUrl)
                //.setScope("email")
                .setCode(code)
                .buildQueryMessage();

        OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
        FacebookTokenResponse oAuthResponse = oAuthClient.accessToken(request, FacebookTokenResponse.class);

        return oAuthResponse;
    }

    public String GenerateAuthUrl() throws OAuthSystemException{
        OAuthClientRequest request = OAuthClientRequest
                .authorizationProvider(OAuthProviderType.FACEBOOK)
                .setClientId(clientId)
                .setRedirectURI(callbackUrl)
                .setScope("email,public_profile")
                .buildQueryMessage();

        return request.getLocationUri();
    }

    public SocialNetworkProfile GetUserId() throws OAuthSystemException, OAuthProblemException{

        if(token=="")
            throw new OAuthSystemException("Token must be set");

        OAuthClientRequest bearerClientRequest = new OAuthBearerClientRequest("https://graph.facebook.com/me?fields=id,first_name,last_name,email")
                .setAccessToken(token).buildQueryMessage();

        OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());

        FacebookMeResponse fbProfile = oAuthClient.resource(bearerClientRequest, OAuth.HttpMethod.GET, FacebookMeResponse.class);

        verifyProfileComplete(fbProfile);

        return fbProfile;
    }

    private void verifyProfileComplete(FacebookMeResponse fbProfile) throws OAuthProblemException{
        if( StringUtils.IsNullOrEmpty(fbProfile.getId())
            //|| StringUtils.IsNullOrEmpty(fbProfile.getEmail())
            || StringUtils.IsNullOrEmpty(fbProfile.getFirstName())
            || StringUtils.IsNullOrEmpty(fbProfile.getLastName())){
             throw OAuthProblemException.error("Facebook profile is not complete");

        }
    }
}
