package com.demo.services.authorization;

import com.demo.services.authorization.exceptions.ProviderNotFoundException;
import com.demo.services.authorization.interfaces.AuthorizationProvider;
import com.demo.services.authorization.interfaces.AuthorizationProviderFactory;
import com.demo.services.authorization.providers.facebook.FacebookProvider;
import com.demo.services.interfaces.Configuration;

import javax.inject.Inject;
import javax.naming.ConfigurationException;

public class DefaultAuthorizationProviderFactory implements AuthorizationProviderFactory{

    @Inject
    Configuration c;

    public AuthorizationProvider GetProvider(Provider provider) throws ProviderNotFoundException, ConfigurationException{
        switch (provider){
            case facebook:
                return new FacebookProvider(c);
        }
        throw new ProviderNotFoundException();
    }
}
