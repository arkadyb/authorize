package com.demo.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.demo.models.entities.Entity;
import com.demo.services.interfaces.Configuration;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoCredential;
import org.bson.BsonSerializationException;
import org.bson.types.ObjectId;
import org.mongojack.Aggregation;
import org.mongojack.DBQuery;
import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;

import javax.inject.Inject;
import javax.naming.ConfigurationException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MongoClient extends ServiceBase {

    private DB db=null;
    private ObjectMapper mapper=null;


    @Inject
    public MongoClient(Configuration c) throws ConfigurationException{
        try {

            List<MongoCredential> credentials = new ArrayList();
            credentials.add(MongoCredential.createCredential(
                    c.getValue("mongouser"),
                    c.getValue("mongodb"),
                    c.getValue("mongopassword").toCharArray()));

            com.mongodb.MongoClient mongo = new com.mongodb.MongoClient(c.getValue("mongohost"), Integer.parseInt(c.getValue("mongoport")));

            db = mongo.getDB(c.getValue("mongodb"));
            mapper = new ObjectMapper();
        } catch (Exception e) {
            throw e;
        }
    }

    public <T extends Entity> boolean ItemExists(Class<T> type, String field, String value){
        try{
            if(db==null)
                throw new NullPointerException("Db object is null.");

            DBCollection coll = db.getCollection(getCollectionName(type));
            JacksonDBCollection jColl = JacksonDBCollection.wrap(coll, type, String.class);

            if(jColl.findOne(DBQuery.is(field,value))!=null)
                return true;
        } catch (Exception e){
            logger.Error(e);
        }
        return false;
    }

    public Entity SaveItem(Entity item){
        try {
            if(db==null)
                throw new NullPointerException("Db object is null.");
            if (!mapper.canSerialize(item.getClass()))
                throw new BsonSerializationException(String.format("Cant serialize object of type %s", item.getClass()));

            DBCollection coll = db.getCollection(getCollectionName(item.getClass()));
            JacksonDBCollection<Entity, ObjectId> jColl = JacksonDBCollection.wrap(coll, Entity.class, ObjectId.class);

            WriteResult<Entity, ObjectId> res = jColl.insert(item);
            item.setEntityId(res.getSavedId());

            return item;

        }catch (Exception e){
            logger.Error(e);
        }

        return null;
    }
//
//    public boolean DeleteItem(Entity item){
//        try{
//
//            if(db==null)
//                throw new NullPointerException("Db object is null.");
//
//            DBCollection coll = db.getCollection(getCollectionName(item.getClass()));
//            JacksonDBCollection<Entity, ObjectId> jColl = JacksonDBCollection.wrap(coll, Entity.class, ObjectId.class);
//
//            jColl.removeById(item.getEntityId());
//
//            return true;
//
//        } catch (Exception e){
//            logger.Error(e);
//        }
//
//        return false;
//    }
//
//    public boolean UpdateItem(Entity item, DBQuery.Query query){
//        try{
//            if(db==null)
//                throw new NullPointerException("Db object is null.");
//            if (!mapper.canSerialize(item.getClass()))
//                throw new BsonSerializationException(String.format("Cant serialize object of type %s", item.getClass()));
//
//            DBCollection coll = db.getCollection(getCollectionName(item.getClass()));
//            JacksonDBCollection<Entity, ObjectId> jColl = JacksonDBCollection.wrap(coll, Entity.class, ObjectId.class);
//
//            jColl.update(query, item);
//
//            return true;
//        }
//        catch (Exception e){
//            logger.Error(e);
//        }
//
//        return false;
//    }
//
    public boolean UpdateItem(Entity item){
        try{
            if(db==null)
                throw new NullPointerException("Db object is null.");
            if (!mapper.canSerialize(item.getClass()))
                throw new BsonSerializationException(String.format("Cant serialize object of type %s", item.getClass()));

            DBCollection coll = db.getCollection(getCollectionName(item.getClass()));
            JacksonDBCollection<Entity, ObjectId> jColl = JacksonDBCollection.wrap(coll, Entity.class, ObjectId.class);

            jColl.updateById(item.getEntityId(), item);

            return true;
        }
        catch (Exception e){
            logger.Error(e);
        }

        return false;
    }
//
//    public <T extends Entity> T GetItem(Class<T> type, String Id){
//        DBQuery.Query query = DBQuery.is("_id", Id);
//
//        List<T> res = getItems(type, query);
//
//        if (!res.isEmpty())
//            return res.get(0);
//
//        return null;
//    }
//
//    public <T extends Entity> List<T> GetItems(Class<T> type, String key, String value) {
//        DBQuery.Query query = DBQuery.is(key, value);
//
//        List<T> res = getItems(type, query);
//        if (!res.isEmpty())
//            return res;
//
//        return null;
//    }
//
    public <T extends Entity> List<T> GetItems(Class<T> type, AbstractMap.SimpleEntry<String, String>[] conditions) throws Exception {
        DBQuery.Query query=DBQuery.empty();

        for (AbstractMap.SimpleEntry<String,String> condition: conditions) {
            query = query.is(condition.getKey(), condition.getValue());
        }

        List<T> res = getItems(type, query);
        if (!res.isEmpty() && res.size()>0)
            return res;

        return null;
    }

//    public <T extends Entity> T GetItem(Class<T> type, AbstractMap.SimpleEntry<String, String>[] conditions) throws Exception{
//        List<T> res = GetItems(type, conditions);
//        if(res!=null && res.size()>0)
//            return res.get(0);
//
//        return null;
//    }
//
//    public <T extends Entity> List<T> GetItems(Class<T> type, DBQuery.Query query) {
//        return getItems(type, query);
//    }
//
//    public <T extends Entity> JacksonDBCollection<T, String> GetCollection(Class<T> type){
//
//        DBCollection coll = db.getCollection(getCollectionName(type));
//        JacksonDBCollection<T, String> jColl = JacksonDBCollection.wrap(coll, type, String.class);
//
//        return jColl;
//    }

    private <T extends Entity> List<T> getItems(Class<T> type, DBQuery.Query query){
        try{
            if(db==null)
                throw new NullPointerException("Db object is null.");

            DBCollection coll = db.getCollection(getCollectionName(type));
            JacksonDBCollection<T, String> jColl = JacksonDBCollection.wrap(coll, type, String.class);
            return jColl.find(query).toArray();
        }catch(Exception e){
            logger.Error(e);
        }

        return null;
    }

    private String getCollectionName(Class type) {
        return String.format("%ss",  type.getSimpleName().toLowerCase());
    }
}
