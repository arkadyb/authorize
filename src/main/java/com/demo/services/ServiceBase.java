package com.demo.services;

import com.demo.services.interfaces.Configuration;
import com.demo.services.interfaces.Logger;

import javax.inject.Inject;

public class ServiceBase {
    @Inject
    Logger logger;

    @Inject
    Configuration conf;
}
