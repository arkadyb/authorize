package com.demo.services;

import com.demo.services.interfaces.Configuration;

import javax.inject.Singleton;
import javax.naming.ConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Singleton
public class PropertiesConfiguration implements Configuration {

    private Properties configuration=new Properties();

    public PropertiesConfiguration(){
        InputStream configFileStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
        try {
            configuration.load(configFileStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getValue(String name) throws ConfigurationException {

        String res=configuration.getProperty(name);

        if (res!="")
            return res;

        throw new ConfigurationException(String.format("Configuration key %s not found.", name));
    }
}
