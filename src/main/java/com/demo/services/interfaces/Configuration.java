package com.demo.services.interfaces;

import javax.naming.ConfigurationException;

public interface Configuration {
    String getValue(String name) throws ConfigurationException;
}
