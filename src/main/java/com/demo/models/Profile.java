package com.demo.models;

import com.demo.models.entities.BasicProfile;
import java.util.ArrayList;
import java.util.List;

/*
    Profile is model class extends BasicProfile with auth properties.
 */
public class Profile extends BasicProfile {

    private String password;
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    private List<AuthToken> tokens;
    public List<AuthToken> getTokens() {
        return tokens;
    }
    public void setTokens(List<AuthToken> tokens) {
        this.tokens = tokens;
    }

    public Profile(){
        this.tokens = new ArrayList<>();
    }
}