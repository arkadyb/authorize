package com.demo.models.entities;

import org.mongojack.Id;
import org.mongojack.ObjectId;

import java.util.Date;

/*
    Entity is base class of MongoDB saved items.
 */
public abstract class Entity{

    private org.bson.types.ObjectId entityId;

    @ObjectId
    @Id
    public org.bson.types.ObjectId getEntityId() {
        return entityId;
    }

    @ObjectId
    @Id
    public void setEntityId(org.bson.types.ObjectId entityId) {
        this.entityId = entityId;
    }

    private Date created;
    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Entity(){
        created = new Date();
    }
}
