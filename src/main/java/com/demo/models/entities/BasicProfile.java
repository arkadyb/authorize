package com.demo.models.entities;

/*
    BasicProfile is base profile implementation, listing minimum set of fields
    used to identify a user.
 */
public class BasicProfile extends Entity{
    private String firstName;

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String lastName;

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String email;

    public String getEmail(){
        return email;
    }
    public void setEmail(String e){
        email=e;
    }
}
