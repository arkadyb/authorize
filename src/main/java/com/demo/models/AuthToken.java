package com.demo.models;

import com.demo.services.authorization.interfaces.AuthorizationProviderFactory;

/*
    AuthToken is model class describes user authorization token.
 */
public class AuthToken {

    private String token;
    private Long expires;

    public Long getExpires(){
        return expires;
    }
    public void setExpires(Long expires){
        this.expires = expires;
    }

    public String getToken(){
        return token;
    }
    public void setToken(String token){
        this.token = token;
    }

    private String socialId;
    private AuthorizationProviderFactory.Provider type;

    public AuthorizationProviderFactory.Provider getType(){
        return type;
    }
    public void setType(AuthorizationProviderFactory.Provider provider){
        this.type = provider;
    }

    public String getSocialId() {
        return socialId;
    }
    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    //Do not remove; required for deserialization frm DB
    public AuthToken(){}

    public AuthToken(String token, Long expires, String socialId, AuthorizationProviderFactory.Provider provider){
        this.token = token;
        this.expires = expires;
        this.type = provider;
        this.socialId = socialId;
    }
}
